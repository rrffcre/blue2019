#include "variables.cpp"
#include "..\include\main.h"

using namespace pros::c;
bool launcher_was_pressed = false;
int liftCurrent;

void auton(void){



}

void userControl(void){
  motor_set_reversed(driveLBPort, 1);
  motor_set_reversed(driveRBPort, 1);

  int power = controller_get_analog(CONTROLLER_MASTER, ANALOG_RIGHT_Y);
  int turn = controller_get_analog(CONTROLLER_MASTER, ANALOG_RIGHT_X);
  int left = power + turn;
  int right = power - turn;
  right *= -1; // This reverses the right motor
  if(left > 127){
    left = 127;
  }
  if(right > 127){
    right = 127;
  }
  if(left < -127){
    left = -127;
  }
  if(right < -127){
    right = -127;
  }
  motor_move(driveLFPort, left);
  motor_move(driveLBPort, left);
  motor_move(driveRFPort, right);
  motor_move(driveRFPort, right);

  if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_R1) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_R1)){
    motor_move_velocity(launcherPort, 200);
  } else if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_R2) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_R2) && launcher_was_pressed == false){
      motor_tare_position(launcherPort);
      launcher_was_pressed = true;
  } else{
    motor_move(launcherPort, 0);
  }

  if(launcher_was_pressed == true && motor_get_position(launcherPort) < 2700){
    motor_move_velocity(launcherPort,200); // Moves 100 units forward
  } else if(launcher_was_pressed == true && motor_get_position(launcherPort) > 2700){
    launcher_was_pressed = false;
  }

  if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_L1) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_L1)){
    motor_move(intakePort, 127);
  } else if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_L2) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_L2)){
    motor_move(intakePort, -127);
  } else{
    motor_move(intakePort, 0);
  }

  if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_UP) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_UP)){
    motor_move_absolute(anglerPort, -1300, 127);
  } else if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_DOWN) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_DOWN)){
    motor_move_absolute(anglerPort, 0, 127);
  } else if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_LEFT) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_LEFT)){
    motor_move_absolute(anglerPort, -1100, 127);
  } else if(controller_get_digital(CONTROLLER_MASTER, DIGITAL_RIGHT) || controller_get_digital(CONTROLLER_PARTNER, DIGITAL_RIGHT)){
    motor_move_absolute(anglerPort, -600, 127);
  }

  int liftControl = controller_get_analog(CONTROLLER_MASTER, ANALOG_LEFT_Y);

  if(liftControl > 127){
    liftControl = 127;
  }
  if(liftControl < -127){
    liftControl = -127;
  }

  if(liftControl > 20 || liftControl < -20){
    motor_move(liftPort, liftControl);
    liftCurrent = motor_get_position(liftPort);
  } else{
    motor_move_absolute(liftPort, liftCurrent, 0);
  }


}

int main(){

  Competition.autonomous(auton);

  Competition.drivercontrol(userControl);

}
