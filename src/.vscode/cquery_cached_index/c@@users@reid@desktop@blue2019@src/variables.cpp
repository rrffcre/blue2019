#include "..\include\main.h"
using namespace pros::c;

#define driveLFPort 11
#define driveLBPort 12
#define driveRFPort 13
#define driveRBPort 14
#define launcherPort 15
#define intakePort 16
#define anglerPort 17
#define liftPort 18
#define joystick_R_Y controller_get_analog(CONTROLLER_MASTER, ANALOG_RIGHT_Y)
#define joystick_R_X controller_get_analog(CONTROLLER_MASTER, ANALOG_RIGHT_X)
#define joystick_L_Y controller_get_analog(CONTROLLER_MASTER, ANALOG_LEFT_Y)
#define joystick_L_X controller_get_analog(CONTROLLER_MASTER, ANALOG_LEFT_X)
#define ENCO 0
#define LNCH 1
