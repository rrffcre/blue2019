#include"..\include\main.h"
#include"..\include\pros\vision.hpp"
#include "variables.cpp"
#include "initialize.cpp"
#include "autonomous.cpp"
#include "opcontrol.cpp"



using namespace pros::c;

while(true){
  while(!competition_is_connected()){
    opcontrol();
  }
  while(competition_is_connected() && competition_is_disabled()){
    competition_initialize();
  }
  while(competition_is_connected() && competition_is_autonomous()){
    autonomous();
  }
  while(competition_is_connected() && !competition_is_disabled() && !competition_is_autonomous()){
    opcontrol();
  }
}
