#include "..\include\main.h"
<<<<<<< HEAD
#include "variables.cpp"
=======
int auto_count;
using namespace pros::c;

// Fill out
typedef enum {
  TIME,
  OTHER,
} Sensor;

void step_end(){
  motor_tare_position(driveLFPort);
  motor_tare_position(driveLBPort);
  motor_tare_position(driveRFPort);
  motor_tare_position(driveRBPort);
  motor_tare_position(launcherPort);
  motor_tare_position(intakePort);
  motor_tare_position(anglerPort);
  auto_count++;
}

void step_calc(int duration, int sensor){
  if(sensor == ENCO){
    if(motor_get_position(driveLFPort) > duration || motor_get_position(driveRFPort) > duration){
      step_end();
    }
  } else if(sensor == LNCH){
    if(motor_get_target_position(launcherPort) = duration){
      step_end();
    }
  }

}

void auto_step(int left, int right, int intake, int angler, int launcher, int duration, int sensor){
  motor_move(driveLFPort, left);
  motor_move(driveLBPort, left);
  motor_move(driveRFPort, right);
  motor_move(driveRBPort, right);
  motor_move(intakePort, intake);
  motor_move_absolute(anglerPort, angler, 127);
  motor_move_relative(launcherPort, launcher, 127);
  step_calc(duration, sensor);


}



void autonomous() {
  if(auto_count == 0){
    switch(auto_count){
      case 0 : auto_step(0, 0, 0, 0, 2700, 2700, LNCH); break;
    }
>>>>>>> 20809000ebec95671412155d580725992ee6c2c6

using namespace pros::c;

<<<<<<< HEAD
void autonomous(){
  motor_move_relative(launcherPort, 2700, 200);
=======
	int **selectedRoutine; //Define a generic 2 dimensional array
	switch(routineNumber) { //need to define this variable
		case 0: selectedRoutine = autonomous0;
		case 1:	selectedRoutine = autonomous1;
		default:	selectedRoutine = nullAuto;
	}

	while (true) {
		int *currentStep = selectedRoutine[auto_count]; //get a one dimensional array of step information
		int linear = currentStep[0];
		int turn = currentStep[1];
		int lift = currentStep[2];
		int launcher = currentStep[3];
		int duration = currentStep[4];
		Sensor sensor = (Sensor) currentStep[5];
		bool end = (bool) currentStep[6];

		//use the data to control the robot.

		//if duration is met increment auto_count;

		if (end) {
			break; //exit the while loop
		}
	}

>>>>>>> 20809000ebec95671412155d580725992ee6c2c6
}

int autonomous0 [][7] { // 7 must match the width of each step
  //Linear, Turn, Lift, Lancher,  Duration, Sensor, End
  { 100,    0,    0,    20,       500,      TIME,   0	}, //step 1
  { 0,      -90,  0,    20,       100,      TIME,   0	}, //step 2
  { 0,			0,		0,		0,				0,				TIME,		1	}, //step end
};

int autonomous1 [][7] { // 7 must match the width of each step
  //Linear, Turn, Lift, Lancher,  Duration, Sensor, End
  { 100,    0,    0,    20,       500,      TIME,   0	}, //step 1
  { 0,      -90,  0,    20,       100,      TIME,   0	}, //step 2
  { 0,			0,		0,		0,				0,				TIME,		1	}, //step end
};

int nullAuto [][7] { // 7 must match the width of each step
  //Linear, Turn, Lift, Lancher,  Duration, Sensor, End
  { 0,			0,		0,		0,				0,				TIME,		1	}, //step end
};
